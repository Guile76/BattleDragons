BattleDragons
La version Donjons et Dragons du c�l�bre Battleships (Touch�-coul�).
Concepteurs : Guillaume LEROY et K�vin DESGROSEILLERS-JOHNSTON

1 - Manuel
2 - Point d'entr�e
3 - Am�liorations apport�es au jeu
4 - TO DO
5 - Probl�mes connus

1 - Manuel

Vous incarnez un dragon Chromatique dans le monde de Faer�n (univers des Royaumes Oubli�s.
Vous luttez pour �tendre votre territoire face � certains dragons embl�matiques de ce monde :
- Ashardalon, l'ancien dragon rouge, ayant remplac� son coeur par un Balor.
- Venomfang, le vicieux dragon vert install� � Thundertree, pr�s de Padhiver.
- Iymrith, le dragon-sorcier bleu, qui explore les ruines de Netheril.

Vous devez �radiquer la cohorte de votre adversaire avant qu'il ne tue vos propres minions.
Pour cela, vous et votre adversaire devrez s�lectionner chacun votre tour une case � attaquer.
Vous pourrez suivre l'�volution des points de vie des cr�atures des deux camps.
Le gagnant sera celui qui extermine tous les monstres ennemis en premier.

2 - Point d'entr�e

Le point d'entr�e se trouve � l'adresse : "src/main/Main.java".

3 - Am�liorations apport�es au jeu

- Menu �volutif en fonction des choix du joueur.
- Police personnalis�e int�gr�e au projet.
- Plusieurs visuels personnalis�s (icone d'application, boutons d'accueil, radio boutons en
 icones de dragons...).
- Les b�teaux originaux ont �t� remplac�s par des monstres de D&D (formes en carr�s de une
� plusieurs cases avec d�coupage automatique de l'image pour dessiner le monstre).
- Le joueur peut entrer le nom de son choix.
- Le joueur peut choisir quel type de dragon il incarne. Le dragon incarn� par l'ordinateur
est d�termin� al�atoirement.
- Les cohortes de monstres et le type de terrain sont d�termin�s en fonction de la couleur du
dragon.
- Les barres de vie des monstres (correspondant au nombre de cases que le monstre occupe)
sont indiqu�es dans le camp de chaque joueur. Elles �voluent en fonction des cases du monstre
touch�es par les attaques de l'adversaire.
- Un manuel est int�gr� au jeu.
- En cas de victoire de l'ordinateur son champ de bataille se r�v�le.
- Augmentation de la taille de la grille 14x14 pour correspondre aux statistiques li�es � la
forme des monstres retenue.
- Musique d'ambiance et sons d'�v�nements.

4 - TO DO

- Factoriser l'ensemble du code d�ja �crit afin de le rendre plus lisible et, surtout,
plus conforme au mod�le MVC.
- Int�grer un mode multijoueur (le menu a �t� pens� dans cette �ventualit�).
- Int�grer le chat entre joueurs (code d�j� con�u).
- Am�liorer l'intelligence artificielle pour proposer divers niveaux de difficult�.

5 - Probl�mes connus

- (rare) Un monstre peut �tre superpos� avec un autre monstre, emp�chant les deux
monstres d'�tre touch�s par la m�me attaque (impossible de battre celui en arri�re plan)