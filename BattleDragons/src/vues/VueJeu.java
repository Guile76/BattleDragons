package vues;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.UIManager;
import javax.swing.border.CompoundBorder;

import controleurs.Controleur;
import enumerations.Camp;
import vues.conteneurs.PanneauAdaptatif;
import vues.conteneurs.VueFenetreAbstract;

public class VueJeu extends VueFenetreAbstract {

	/**
	 * @author Guillaume LEROY
	 * Fenêtre du jeu
	 */
	private static final long serialVersionUID = -58750718610937224L;
	// Variables
	private JPanel panneauPrincipal;
	private JPanel panneauCentral;
	private PanneauAdaptatif grilleJoueur;
	private PanneauAdaptatif grilleAdversaire;
	private static final int COTES_GRILLES = 14;

	private JPanel panneauJoueurs;
	private JButton boutonJoueur;
	private JButton boutonAdversaire;
	private JLabel nomJoueur;
	private JLabel nomAdversaire;
	private JButton boutonVS;

	private JPanel panneauDiscussion;
	private JScrollPane affichagePanneauDeroulant;
	private JList<String> affichageListe;
	private DefaultListModel<String> affichageContenu;
//	private JPanel panneauSaisie;
//	private JTextArea saisie;
//	private JButton boutonEnvoyer;

	private JPanel panneauJoueur;
	private JPanel panneauAdversaire;

	private Dimension dimensionPanneauxLateraux = new Dimension(100, 0);

	// Constructeur
	public VueJeu(Controleur monControleur) {
		super(monControleur, 1200, 700);

		panneauPrincipal = new JPanel(new BorderLayout());
		panneauPrincipal.setBackground(Color.BLACK);

		genererMenu(monControleur);

		// Panneau central
		panneauCentral = new JPanel(new GridLayout(1, 2, 10, 0));
		panneauCentral.setBackground(Color.BLACK);
		panneauCentral.setBorder(new CompoundBorder(
				BorderFactory.createMatteBorder(3, 3, 3, 3, Color.LIGHT_GRAY),
				BorderFactory.createMatteBorder(3, 3, 3, 3, Color.DARK_GRAY)));

		// Création de la grille du joueur en fontion de son camp
		switch(monControleur.getMonJeu().getJoueur().getCamp()) {
		case ROUGE:
			setGrilleJoueur(new PanneauAdaptatif(new GridLayout(VueJeu.getCotesGrilles(), VueJeu.getCotesGrilles()),"images/rouge/land.png", monControleur));
			getGrilleJoueur().dessinerGrille(VueJeu.getCotesGrilles());
			break;
		case VERT:
			setGrilleJoueur(new PanneauAdaptatif(new GridLayout(VueJeu.getCotesGrilles(), VueJeu.getCotesGrilles()),"images/vert/land.png", monControleur));
			getGrilleJoueur().dessinerGrille(VueJeu.getCotesGrilles());			
			break;
		case BLEU:
			setGrilleJoueur(new PanneauAdaptatif(new GridLayout(VueJeu.getCotesGrilles(), VueJeu.getCotesGrilles()),"images/bleu/land.png", monControleur));
			getGrilleJoueur().dessinerGrille(VueJeu.getCotesGrilles());			
			break;
		}

		// Création de la grille de l'adversaire en fonction de son camp
		switch(monControleur.getMonJeu().getAdversaire().getCamp()) {
		case ROUGE:
			setGrilleAdversaire(new PanneauAdaptatif(new GridLayout(VueJeu.getCotesGrilles(), VueJeu.getCotesGrilles()),"images/rouge/land.png", monControleur));
			getGrilleAdversaire().dessinerGrille(VueJeu.getCotesGrilles());			
			break;
		case VERT:
			setGrilleAdversaire(new PanneauAdaptatif(new GridLayout(VueJeu.getCotesGrilles(), VueJeu.getCotesGrilles()),"images/vert/land.png", monControleur));
			getGrilleAdversaire().dessinerGrille(VueJeu.getCotesGrilles());
			break;
		case BLEU:
			setGrilleAdversaire(new PanneauAdaptatif(new GridLayout(VueJeu.getCotesGrilles(), VueJeu.getCotesGrilles()),"images/bleu/land.png", monControleur));
			getGrilleAdversaire().dessinerGrille(VueJeu.getCotesGrilles());
			break;
		}

		panneauCentral.add(grilleJoueur);
		panneauCentral.add(grilleAdversaire);

		// PANNEAU JOUEURS
		int hauteurPanneauJoueurs = 100;
		int difference = 10;

		panneauJoueurs = new JPanel();
		panneauJoueurs.setBackground(Color.BLACK);
		panneauJoueurs.setPreferredSize(new Dimension(900, hauteurPanneauJoueurs));

		boutonJoueur = new JButton();
		transformerBouton(boutonJoueur);
		boutonJoueur.setPreferredSize(new Dimension(hauteurPanneauJoueurs - difference, hauteurPanneauJoueurs - difference));

		boutonAdversaire = new JButton();
		transformerBouton(boutonAdversaire);
		boutonAdversaire.setPreferredSize(new Dimension(hauteurPanneauJoueurs - difference, hauteurPanneauJoueurs - difference));

		nomJoueur = new JLabel("Joueur1", JLabel.LEFT);
		nomJoueur.setFont(policePersonnalisee(50));
		nomJoueur.setForeground(Color.WHITE);
		nomJoueur.setPreferredSize(new Dimension(350, hauteurPanneauJoueurs - difference));

		nomAdversaire = new JLabel("Joueur2", JLabel.RIGHT);
		nomAdversaire.setFont(policePersonnalisee(50));
		nomAdversaire.setForeground(Color.WHITE);
		nomAdversaire.setPreferredSize(new Dimension(350, hauteurPanneauJoueurs - difference));

		boutonVS = new JButton();
		transformerBouton(boutonVS);
		boutonVS.setPreferredSize(new Dimension(200, hauteurPanneauJoueurs - difference));
		ImageIcon imageVS = new ImageIcon("images/Illustrations/03_VS.png");		
		ImageIcon iconeVS = new ImageIcon(imageVS.getImage().getScaledInstance(100, hauteurPanneauJoueurs - 4, java.awt.Image.SCALE_SMOOTH));
		boutonVS.setIcon(iconeVS);
		boutonVS.setDisabledIcon(iconeVS);

		panneauJoueurs.add(nomJoueur);
		panneauJoueurs.add(boutonJoueur);
		panneauJoueurs.add(boutonVS);
		panneauJoueurs.add(boutonAdversaire);
		panneauJoueurs.add(nomAdversaire);

		// Panneau INFORMATIONS - CHAT
		panneauDiscussion = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 1));
		panneauDiscussion.setBackground(Color.BLACK);
		panneauDiscussion.setPreferredSize(new Dimension(1190, 100));

		affichageContenu = new DefaultListModel<>();
		affichageListe = new JList<>(affichageContenu);
		
		
		affichagePanneauDeroulant = new JScrollPane(affichageListe);
		affichagePanneauDeroulant.setBackground(Color.LIGHT_GRAY);
		affichagePanneauDeroulant.setOpaque(true);
		affichagePanneauDeroulant.setPreferredSize(new Dimension(1174, 96));
		affichagePanneauDeroulant.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

//		panneauSaisie = new JPanel();
//		panneauSaisie.setPreferredSize(new Dimension(1180, 30));
//		panneauSaisie.setBackground(Color.BLACK);
//		saisie = new JTextArea();
//		saisie.setBackground(Color.LIGHT_GRAY);
//		saisie.setPreferredSize(new Dimension(1091, 24));
//		saisie.setFont(policePersonnalisee(18));
//		boutonEnvoyer = new JButton("Envoyer");
//		panneauSaisie.add(saisie);
//		panneauSaisie.add(boutonEnvoyer);

		panneauDiscussion.add(affichagePanneauDeroulant);
//		panneauDiscussion.add(panneauSaisie);	
		
		// PANNEAUX LATERAUX

		panneauJoueur = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 5));
		panneauAdversaire = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 5));

		remplirPanneauLateral(panneauJoueur, monControleur.getCampJoueur());
		remplirPanneauLateral(panneauAdversaire, monControleur.getCampAdversaire());

		// Implémentation des panneaux
		panneauPrincipal.add(panneauJoueurs, BorderLayout.NORTH);
		panneauPrincipal.add(panneauCentral, BorderLayout.CENTER);
		panneauPrincipal.add(panneauDiscussion, BorderLayout.SOUTH);
		panneauPrincipal.add(panneauJoueur, BorderLayout.WEST);
		panneauPrincipal.add(panneauAdversaire, BorderLayout.EAST);

		setContentPane(panneauPrincipal);
		setVisible(true);
	}

	// Accesseurs et Mutateurs
	public PanneauAdaptatif getGrilleJoueur() {
		return grilleJoueur;
	}

	public PanneauAdaptatif getGrilleAdversaire() {
		return grilleAdversaire;
	}

	public void setGrilleJoueur(PanneauAdaptatif grilleJoueur) {
		this.grilleJoueur = grilleJoueur;
	}

	public void setGrilleAdversaire(PanneauAdaptatif grilleAdversaire) {
		this.grilleAdversaire = grilleAdversaire;
	}

	public void setIconeJoueur(String lien) {		
		ImageIcon imageTemp = new ImageIcon(lien);
		ImageIcon icone = new ImageIcon(imageTemp.getImage().getScaledInstance(boutonJoueur.getWidth(), boutonJoueur.getHeight(), java.awt.Image.SCALE_SMOOTH));		
		boutonJoueur.setIcon(icone);
		boutonJoueur.setDisabledIcon(icone);
	}

	public void setIconeAdversaire(String lien) {		
		ImageIcon imageTemp = new ImageIcon(lien);
		ImageIcon icone = new ImageIcon(imageTemp.getImage().getScaledInstance(boutonAdversaire.getWidth(), boutonAdversaire.getHeight(), java.awt.Image.SCALE_SMOOTH));		
		boutonAdversaire.setIcon(icone);
		boutonAdversaire.setDisabledIcon(icone);
	}

	public void setNomJoueur(String nouveauNom, Camp camp) {
		nomJoueur.setText(nouveauNom);
		switch(camp) {
		case ROUGE:
			nomJoueur.setForeground(new Color(150, 0, 0));
			break;
		case VERT:
			nomJoueur.setForeground(new Color(0, 100, 0));
			break;
		case BLEU:
			nomJoueur.setForeground(new Color(45, 71, 115));
			break;
		}
	}

	public void setNomAdversaire(String nouveauNom, Camp camp) {
		nomAdversaire.setText(nouveauNom);
		switch(camp) {
		case ROUGE:
			nomAdversaire.setForeground(new Color(150, 0, 0));
			break;
		case VERT:
			nomAdversaire.setForeground(new Color(0, 100, 0));
			break;
		case BLEU:
			nomAdversaire.setForeground(new Color(45, 71, 115));
			break;
		}
	}

	public boolean isBoutonActifJoueur(int indexBouton) {
		Object[] tableauBoutons = grilleJoueur.getComponents();
		return ((JButton) tableauBoutons[indexBouton]).isEnabled();
	}

	public boolean isBoutonActifAdversaire(int indexBouton) {
		Object[] tableauBoutons = grilleAdversaire.getComponents();
		return ((JButton) tableauBoutons[indexBouton]).isEnabled();
	}

	public JButton getBoutonJoueur(int indexBouton) {
		Component[] tableauBoutons = grilleJoueur.getComponents();
		return (JButton) tableauBoutons[indexBouton];
	}

	public JButton getBoutonAdversaire(int indexBouton) {
		Component[] tableauBoutons = grilleAdversaire.getComponents();
		return (JButton) tableauBoutons[indexBouton];
	}

	public static int getCotesGrilles() {
		return COTES_GRILLES;
	}

	public JProgressBar getProgressBarMonstreAdverse(int indexMonstre) {
		JProgressBar barreTrouvee = new JProgressBar();
		Component[] objetsPanneau = panneauAdversaire.getComponents();

		for(int compteur = 0; compteur < objetsPanneau.length; compteur++) {
			if(objetsPanneau[compteur] instanceof JProgressBar) {
				JProgressBar temp = (JProgressBar) objetsPanneau[compteur];
				if(temp.getName().equals("Monstre " + indexMonstre)) {
					barreTrouvee = temp;
				}	
			}
		}
		return barreTrouvee;
	}

	public JProgressBar getProgressBarMonstreJoueur(int indexMonstre) {
		JProgressBar barreTrouvee = new JProgressBar();
		Component[] objetsPanneau = panneauJoueur.getComponents();

		for(int compteur = 0; compteur < objetsPanneau.length; compteur++) {
			if(objetsPanneau[compteur] instanceof JProgressBar) {
				JProgressBar temp = (JProgressBar) objetsPanneau[compteur];
				if(temp.getName().equals("Monstre " + indexMonstre)) {
					barreTrouvee = temp;
				}	
			}
		}
		return barreTrouvee;
	}

	// Méthodes	
	public void genererMenu(Controleur monControleur) {
		JMenuBar barreMenu = new JMenuBar();
		JMenu menuJeu = new JMenu("Jeu");
		JMenuItem retourAccueil = new JMenuItem("Accueil");
		retourAccueil.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				monControleur.basculerVueAccueil();
			}
		});
		JMenuItem quitter = new JMenuItem("Quitter");
		quitter.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(DISPOSE_ON_CLOSE);
			}
		});

		menuJeu.add(retourAccueil);
		menuJeu.add(quitter);
		barreMenu.add(menuJeu);
		setJMenuBar(barreMenu);
		barreMenu.setVisible(true);
	}

	public void placerMonstre(PanneauAdaptatif grille, int caseMonstre, String lienImage) {
		JButton bouton = new JButton();
		bouton = grille.getBouton(caseMonstre);
		ImageIcon image = new ImageIcon(lienImage);
		ImageIcon icone = new ImageIcon(image.getImage().getScaledInstance(bouton.getWidth(), bouton.getHeight(), java.awt.Image.SCALE_SMOOTH));

		bouton.setIcon(icone);
	}

	public void placerMonstre(PanneauAdaptatif grille, int caseMonstre, BufferedImage imageMonstre) {
		JButton bouton = new JButton();
		bouton = grille.getBouton(caseMonstre);
		ImageIcon icone = new ImageIcon(imageMonstre.getScaledInstance(bouton.getWidth(), bouton.getHeight(), java.awt.Image.SCALE_SMOOTH));

		bouton.setIcon(icone);
	}

	public void remplirPanneauLateral(JPanel panneau, Camp camp) {
		panneau.setBackground(Color.BLACK);
		panneau.setPreferredSize(dimensionPanneauxLateraux);

		int tailleIconeBouton = 78;

		for(int compteur = 0; compteur < 4; compteur++) {
			JButton bouton = new JButton();
			bouton.setActionCommand("" + compteur);
			bouton.setPreferredSize(new Dimension(90, 81));
			transformerBouton(bouton);
			int vie = 0;

			String dossier = "";
			switch(camp) {
			case ROUGE:
				dossier = "images/rouge/";
				break;
			case VERT:
				dossier = "images/vert/";
				break;
			case BLEU:
				dossier = "images/bleu/";
			}

			switch(compteur) {
			case 0:
				ImageIcon image4 = new ImageIcon(dossier + "1X1.png");
				ImageIcon icone4 = new ImageIcon(image4.getImage().getScaledInstance(tailleIconeBouton, tailleIconeBouton, java.awt.Image.SCALE_SMOOTH));
				bouton.setIcon(icone4);
				bouton.setDisabledIcon(icone4);
				vie = 1;
				break;
			case 1:
				ImageIcon image2 = new ImageIcon(dossier + "2X2A.png");
				ImageIcon icone2 = new ImageIcon(image2.getImage().getScaledInstance(tailleIconeBouton, tailleIconeBouton, java.awt.Image.SCALE_SMOOTH));
				bouton.setIcon(icone2);
				bouton.setDisabledIcon(icone2);
				vie = 4;
				break;
			case 2:
				ImageIcon image3 = new ImageIcon(dossier + "2X2B.png");
				ImageIcon icone3 = new ImageIcon(image3.getImage().getScaledInstance(tailleIconeBouton, tailleIconeBouton, java.awt.Image.SCALE_SMOOTH));
				bouton.setIcon(icone3);
				bouton.setDisabledIcon(icone3);
				vie = 4;
				break;
			case 3:
				ImageIcon image1 = new ImageIcon(dossier + "3X3.png");
				ImageIcon icone1 = new ImageIcon(image1.getImage().getScaledInstance(tailleIconeBouton, tailleIconeBouton, java.awt.Image.SCALE_SMOOTH));
				bouton.setIcon(icone1);
				bouton.setDisabledIcon(icone1);
				vie = 9;
			}

			UIManager.put("ProgressBar.selectionForeground", new Color(200, 255, 255)); 
			JProgressBar barreVie = new JProgressBar(0, 0, vie);
			barreVie.setName("Monstre " + compteur);
			barreVie.setValue(vie);
			barreVie.setForeground(new Color(200, 0, 0));
			barreVie.setPreferredSize(new Dimension(90, 20));
			barreVie.setString(barreVie.getValue() + " / " + barreVie.getMaximum());
			barreVie.setStringPainted(true);

			panneau.add(bouton);
			panneau.add(barreVie);
		}		
	}

	public void transformerBouton(JButton bouton) {
		bouton.setEnabled(false);
		bouton.setContentAreaFilled(false);
		bouton.setBorderPainted(false);
	}
	
	public void ajouterTexteLog(String texte) {
			affichageContenu.addElement(texte);
			affichageListe.ensureIndexIsVisible(affichageListe.getModel().getSize() - 1);
	}
}
