package vues.composants;

import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 * 
 * @author Guillaume LEROY
 * Classe personnalisée pour lire les sons
 */
public class LecteurDeSon {

	// Variables
	AudioInputStream monAudio;
	Clip monClip;

	// Constructeur
	public LecteurDeSon() {

	}

	// Méthodes
	public void jouer(String adresseFichierSon, boolean loop) {

		try {
			monAudio = AudioSystem.getAudioInputStream(new File(adresseFichierSon));
			monClip = AudioSystem.getClip();
		} catch (UnsupportedAudioFileException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (LineUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		Thread tSon = new Thread(new MonRunnable(monClip, monAudio, loop));
		tSon.start();
	}


}

class MonRunnable implements Runnable {

	// Variables
	AudioInputStream monAudio;
	Clip monClip;
	boolean loop = false;

	public MonRunnable(Clip monClip, AudioInputStream monAudio, boolean loop) {
		this.monClip = monClip;
		this.monAudio = monAudio;
	}

	@Override
	public void run() {
		try {
			monClip.open(monAudio);
			monClip.start();
		} catch (LineUnavailableException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
