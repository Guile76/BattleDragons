package vues.composants;

import java.awt.Color;

import javax.swing.JButton;

import vues.conteneurs.VueFenetreAbstract;

public class BoutonMenu extends JButton {
	
	/**
	 * @author Guillaume LEROY
	 * Classe de boutons de menu personnalisés
	 */
	private static final long serialVersionUID = -1236126856699453429L;

	// Constructeur
	public BoutonMenu(String text, VueFenetreAbstract vueEnCours) {
		super(text);
		setFont(vueEnCours.policePersonnalisee(25));
		setBorderPainted(false);
		setBackground(new Color(0, 0, 0));
		setForeground(new Color(200, 50, 0));
	}
}
