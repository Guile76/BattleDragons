package vues.conteneurs;

import java.awt.Font;
import java.awt.FontFormatException;
import java.io.File;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JFrame;

import controleurs.Controleur;

public abstract class VueFenetreAbstract extends JFrame {
	
	/**
	 * @author Guillaume LEROY
	 * Classe abstraite pour les différentes fenêtres du jeu
	 */
	private static final long serialVersionUID = 8391310585839383162L;
	// Variables
	private Controleur monControleur;
	private Font fontPersonnalisee = new Font(null);
		
	// Constructeur
	public VueFenetreAbstract(Controleur moncontroleur, int largeur, int hauteur)  {
		super("Battleship, D&D Edition");		
		this.monControleur = monControleur;

		// Caractéristiques de la fenêtre
		setSize(largeur, hauteur);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setLocationRelativeTo(null);
		setResizable(false);
		setIconImage(new ImageIcon("images/Illustrations/02_dnd.png").getImage());
		
		try {
			fontPersonnalisee = Font.createFont(Font.TRUETYPE_FONT, new File("ressources/Mr.EavesSmallCaps.ttf"));
		} catch (FontFormatException | IOException e) {
			e.printStackTrace();
		}
	}

	public Controleur getMonControleur() {
		return monControleur;
	}
	
	public Font policePersonnalisee(float taille) {
		return fontPersonnalisee.deriveFont(taille);
	}
	
}
