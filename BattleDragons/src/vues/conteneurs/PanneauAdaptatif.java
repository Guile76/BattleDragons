package vues.conteneurs;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.LayoutManager;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

import controleurs.Controleur;
import ecouteurs.EcouteurBoutonGrille;

public class PanneauAdaptatif extends JPanel {
	
	/**
	 * @author Guillaume LEROY
	 * Panneau personnalisé pour affichage d'image de fond ou création de
	 * grille de boutons
	 */
	private static final long serialVersionUID = 7838416584124514404L;
	// Variables
	private Image image;
	private JButton bouton;
	private Controleur controleur;
	
	// Constructeurs	
	public PanneauAdaptatif(String adresseImage, int largeur, int hauteur) {
		super();
		image = new ImageIcon(adresseImage).getImage();
		Dimension dimension = new Dimension(largeur, hauteur);
		this.setPreferredSize(dimension);
	}
	
	public PanneauAdaptatif(LayoutManager layout, String adresseImage, Controleur controleur) {
		super(layout);
		image = new ImageIcon(adresseImage).getImage();
		this.controleur = controleur;
	}
	
	// Overrides
	@Override
	protected void paintComponent(Graphics g) {
		g.drawImage(image, 0, 0, PanneauAdaptatif.this.getWidth(), PanneauAdaptatif.this.getWidth(), null);
	}
	
	// Accesseurs et Mutateurs

	public Image getImage() {
		return image;
	}

	public void setImage(String adresseImage) {
		image = new ImageIcon(adresseImage).getImage();
	}

	public JButton getBouton(int index) {
		JButton boutonATrouver = new JButton();
		Component[] composants = this.getComponents();
		boolean trouve = false;
		int compteur = 0;
		while(!trouve && compteur < composants.length) {
			if(composants[compteur] instanceof JButton) {
				JButton bouton = (JButton) composants[compteur];				
				if(bouton.getActionCommand().equals("" + index)) {
					boutonATrouver = bouton;
					trouve = true;
				}
			}
			compteur++;
		}
		
		return boutonATrouver;
	}

	public void setImage(Image image) {
		this.image = image;
	}
	
	// Méthodes
	public void dessinerGrille(int coteGrille) {
		for(int compteur = 0; compteur < (coteGrille * coteGrille); compteur++) {
			bouton = new JButton();
			bouton.setActionCommand("" + compteur);
			bouton.setContentAreaFilled(false);
			
			bouton.addActionListener(new EcouteurBoutonGrille(controleur));
			
			this.add(bouton);
		}
	}
	
}
