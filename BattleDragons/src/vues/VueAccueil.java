package vues;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import controleurs.Controleur;
import enumerations.Camp;
import vues.composants.BoutonMenu;
import vues.composants.LecteurDeSon;
import vues.conteneurs.PanneauAdaptatif;
import vues.conteneurs.VueFenetreAbstract;

public class VueAccueil extends VueFenetreAbstract {

	/**
	 * @author Guillaume LEROY
	 * Vue d'accueil du jeu avec les menus
	 */
	private static final long serialVersionUID = 5455376509211443458L;
	// Variables
	private JPanel panneauPrincipal;
	private JLabel panneauTitre;
	private PanneauAdaptatif panneauImagePrincipale;
	private JPanel panneauBoutons;
	private BoutonMenu boutonSolo;
	private BoutonMenu boutonDuo;
	private BoutonMenu boutonQuitter;
	private BoutonMenu boutonServeur;
	private BoutonMenu boutonClient;
	private BoutonMenu boutonRetour;
	private JPanel radioDragons;
	private BoutonMenu boutonNom;
	private BoutonMenu boutonManuel;
	private LecteurDeSon lecteurSon = new LecteurDeSon();

	// Constructeur
	public VueAccueil(Controleur monControleur) {
		super(monControleur, 500, 700);

		panneauPrincipal = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 10));
		panneauPrincipal.setBackground(Color.BLACK);

		panneauTitre = new JLabel("Battleship, D&D Edition", JLabel.CENTER);
		panneauTitre.setPreferredSize(new Dimension(500, 50));
		panneauTitre.setFont(policePersonnalisee(40));
		panneauTitre.setForeground(new Color(204, 51, 0));

		panneauImagePrincipale = new PanneauAdaptatif("images/Illustrations/01_Battle_Dragons.png", 400, 400);

		panneauBoutons = new JPanel(new GridLayout(4, 1, 0, 5));
		panneauBoutons.setBackground(Color.BLACK);

		if(monControleur.getNomJoueur().equals("") || monControleur.getNomJoueur() == null) {
			boutonNom = new BoutonMenu("Votre nom", this);
		} else {
			boutonNom = new BoutonMenu(monControleur.getNomJoueur(), this);
		}
		
		boutonSolo = new BoutonMenu("Choisissez votre race", this);
		boutonManuel = new BoutonMenu("Manuel", this);
		boutonDuo = new BoutonMenu("Multiplayer", this);
		boutonQuitter = new BoutonMenu("Quitter", this);
		boutonServeur = new BoutonMenu("Serveur", this);
		boutonClient = new BoutonMenu("Client", this);
		boutonRetour = new BoutonMenu("Retour", this);
		
		VueAccueil.class.getResourceAsStream("images/rouge/dragon.png");

		ImageIcon iconeRouge = new ImageIcon("images/rouge/dragon.png");
		ImageIcon rouge = new ImageIcon(iconeRouge.getImage().getScaledInstance(50, 50, java.awt.Image.SCALE_SMOOTH));
		JRadioButton boutonRouge = new JRadioButton(rouge);
		boutonRouge.setOpaque(false);
		boutonRouge.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				lecteurSon.jouer("sons/Dragon.wav", false);
				monControleur.reinitialiserPartie(monControleur.getNomJoueur(), Camp.ROUGE);
				monControleur.basculerVueJeu();
			}
		});
		ImageIcon iconeVerte = new ImageIcon("images/vert/dragon.png");
		ImageIcon vert = new ImageIcon(iconeVerte.getImage().getScaledInstance(50, 50, java.awt.Image.SCALE_SMOOTH));
		JRadioButton boutonVert = new JRadioButton(vert);
		boutonVert.setOpaque(false);
		boutonVert.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				lecteurSon.jouer("sons/Dragon.wav", false);
				monControleur.reinitialiserPartie(monControleur.getNomJoueur(), Camp.VERT);
				monControleur.basculerVueJeu();
			}
		});
		ImageIcon iconeBleu = new ImageIcon("images/bleu/dragon.png");
		ImageIcon bleu = new ImageIcon(iconeBleu.getImage().getScaledInstance(50, 50, java.awt.Image.SCALE_SMOOTH));
		JRadioButton boutonBleu = new JRadioButton(bleu);
		boutonBleu.setOpaque(false);
		boutonBleu.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				lecteurSon.jouer("sons/Dragon.wav", false);
				monControleur.reinitialiserPartie(monControleur.getNomJoueur(), Camp.BLEU);
				monControleur.basculerVueJeu();
			}
		});

		ButtonGroup groupeRadios = new ButtonGroup();
		groupeRadios.add(boutonBleu);
		groupeRadios.add(boutonVert);
		groupeRadios.add(boutonRouge);

		radioDragons = new JPanel();
		radioDragons.setOpaque(false);
		radioDragons.add(boutonRouge);
		radioDragons.add(boutonVert);
		radioDragons.add(boutonBleu);

		boutonSolo.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(monControleur.getNomJoueur().equals("") || monControleur.getNomJoueur() == null) {
					JOptionPane.showMessageDialog(null, "Entrez votre nom avant de pouvoir jouer.", "Nom à définir", JOptionPane.INFORMATION_MESSAGE);
				} else {
					accederPanneauSaisieJoueur();
				}

			}
		});

		boutonManuel.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				afficherManuel();
			}
		});

		boutonDuo.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				accederPanneauReseau();
			}
		});

		boutonQuitter.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(DISPOSE_ON_CLOSE);
			}
		});

		boutonRetour.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				accederPanneauAccueil();
			}
		});

		boutonNom.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JButton bouton = (JButton) e.getSource();
				String reponse = "";
				do{
					reponse = JOptionPane.showInputDialog(null, "Entrez votre nom :", "Saisie du nom", JOptionPane.QUESTION_MESSAGE);
				}while(reponse == null || reponse.equals(""));
				monControleur.setNomJoueur(reponse);
				bouton.setText(monControleur.getNomJoueur());
			}
		});

		panneauBoutons.add(boutonNom);
		panneauBoutons.add(boutonSolo);
		panneauBoutons.add(boutonManuel);
		panneauBoutons.add(boutonQuitter);

		panneauPrincipal.add(panneauTitre);
		panneauPrincipal.add(panneauImagePrincipale);
		panneauPrincipal.add(panneauBoutons);

		setContentPane(panneauPrincipal);		
		setVisible(true);

	}

	// Accesseurs et Mutateurs
	public JPanel getPanneauBoutons() {
		return panneauBoutons;
	}

	// Méthodes
	public void accederPanneauReseau() {
		panneauBoutons.removeAll();
		panneauBoutons.add(boutonServeur);
		panneauBoutons.add(boutonClient);
		panneauBoutons.add(boutonRetour);
		panneauBoutons.revalidate();
	}

	public void accederPanneauAccueil() {
		panneauBoutons.removeAll();
		panneauBoutons.add(boutonNom);
		panneauBoutons.add(boutonSolo);
		panneauBoutons.add(boutonManuel);
		panneauBoutons.add(boutonQuitter);
		panneauBoutons.revalidate();
	}

	public void accederPanneauSaisieJoueur() {
		panneauBoutons.removeAll();
		panneauBoutons.add(radioDragons);
		panneauBoutons.add(boutonRetour);

		panneauBoutons.revalidate();
	}

	public void afficherManuel() {
		BufferedImage imageBase;
		ImageIcon icone = new ImageIcon();
		try {
			imageBase = ImageIO.read(new File("images/Illustrations/02_dnd.png"));
			icone = new ImageIcon(imageBase.getScaledInstance(50, 50, java.awt.Image.SCALE_SMOOTH));

		} catch (IOException e) {
			e.printStackTrace();
		}
		String message = "Bienvenue dans la version Donjons et Dragons du célèbre jeu Battleships (Touché-Coulé)\n"
				+ "\n"
				+ "Vous incarnez un dragon Chromatique dans le monde de Faerûn (univers des Royaumes Oubliés.\n"
				+ "Vous luttez pour étendre votre territoire face à certains dragons emblématiques "
				+ "de ce monde :\n"
				+ "- Ashardalon, l'ancien dragon rouge, ayant remplacé son coeur par un Balor.\n"
				+ "- Venomfang, le vicieux dragon vert installé à Thundertree, près de Padhiver.\n"
				+ "- Iymrith, le dragon-sorcier bleu, qui explore les ruines de Netheril.\n"
				+ "\n"
				+ "Vous devez éradiquer la cohorte de votre adversaire avant qu'il ne tue vos propres "
				+ "minions.\n"
				+ "Pour cela, vous et votre adversaire devrez sélectionner chacun votre tour une case à attaquer.\n"
				+ "Vous pourrez suivre l'évolution des points de vie des créatures des deux camps.\n"
				+ "Le gagnant sera celui qui extermine tous les monstres ennemis en premier.";

		JOptionPane.showMessageDialog(this.getParent(), message, "Manuel", JOptionPane.INFORMATION_MESSAGE, icone);
	}
}
