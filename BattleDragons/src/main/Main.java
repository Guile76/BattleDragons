package main;

import javax.swing.SwingUtilities;

import controleurs.Controleur;
import modeles.Jeu;

public class Main {

	public static void main(String[] args) {
		SwingUtilities.invokeLater( new Runnable() {
			public void run() {
				new Controleur(new Jeu());
			}
		} );
	}

}
