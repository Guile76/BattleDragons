package ecouteurs;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;

import controleurs.Controleur;
import vues.conteneurs.PanneauAdaptatif;

/**
 * @author Guillaume LEROY Classe d'ecouteur pour les boutons des grilles de
 * joueurs
 */
public class EcouteurBoutonGrille implements ActionListener {

    // Variables
    Controleur controleur;

    // Constructeur
    public EcouteurBoutonGrille(Controleur controleur) {
        this.controleur = controleur;
    }

    // Overrides
    @Override
    public void actionPerformed(ActionEvent e) {
        JButton bouton = (JButton) e.getSource();

        // Obtention du numero de bouton
        String identifiantBouton = bouton.getActionCommand();
        int numeroBouton = Integer.parseInt(identifiantBouton);

        // Verification de touche sur la grille adverse
        if (controleur.isTourJoueur() && !controleur.isPanneauJoueur((PanneauAdaptatif) bouton.getParent())) {
            if (bouton.isEnabled()) {
                ImageIcon icone = new ImageIcon();
                if (controleur.isCaseAdversaireOccupee(numeroBouton)) {
                    icone = new ImageIcon(((new ImageIcon("images/Illustrations/04_touche.png")).getImage()).getScaledInstance(bouton.getWidth(),
                            bouton.getHeight(), java.awt.Image.SCALE_SMOOTH));
                    controleur.baisserVieMonstreAdverse(numeroBouton);
                    controleur.getLecteurSon().jouer("sons/Souffrance.wav", false);
                    controleur.afficherTexte("Vous avez touché un monstre ennemi.");
                } else {
                    switch (controleur.getCampJoueur()) {
                        case ROUGE:
                            icone = new ImageIcon(((new ImageIcon("images/rouge/effect.png")).getImage()).getScaledInstance(bouton.getWidth(),
                                    bouton.getHeight(), java.awt.Image.SCALE_SMOOTH));
                            break;
                        case VERT:
                            icone = new ImageIcon(((new ImageIcon("images/vert/effect.png")).getImage()).getScaledInstance(bouton.getWidth(),
                                    bouton.getHeight(), java.awt.Image.SCALE_SMOOTH));
                            break;
                        case BLEU:
                            icone = new ImageIcon(((new ImageIcon("images/bleu/effect.png")).getImage()).getScaledInstance(bouton.getWidth(),
                                    bouton.getHeight(), java.awt.Image.SCALE_SMOOTH));
                    }
                    controleur.afficherTexte("Vous n'avez touché aucune cible.");
                }
                bouton.setIcon(icone);
                bouton.setDisabledIcon(icone);
                bouton.setEnabled(false);
                controleur.setTourJoueur(false);
                controleur.tirAdverseAleatoire();
                controleur.setTourJoueur(true);
                controleur.verifierVictoire();
            }
        }
    }
}
