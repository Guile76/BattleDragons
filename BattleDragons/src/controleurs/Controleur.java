package controleurs;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import enumerations.Camp;
import modeles.Jeu;
import vues.VueAccueil;
import vues.VueJeu;
import vues.composants.LecteurDeSon;
import vues.conteneurs.PanneauAdaptatif;
/**
 * 
 * @author Guillaume LEROY
 * Controleur principal
 */
public class Controleur {
	// Variables
	private VueAccueil fenetreAccueil;
	private VueJeu fenetreJeu;
	private Jeu monJeu; 
	private boolean partieFinie = false;
	private boolean tourJoueur = true;
	private String nomJoueur = "";
	AudioInputStream monAudio;
	Clip monClip;
	private LecteurDeSon lecteurSon = new LecteurDeSon();

	// Constructeur
	public Controleur(Jeu monJeu) {
		this.monJeu = monJeu;
		fenetreAccueil = new VueAccueil(this);
		
		try {
			monAudio = AudioSystem.getAudioInputStream(new File("sons/Theme.wav"));
			monClip = AudioSystem.getClip();
			monClip.open(monAudio);
			monClip.start();
			monClip.loop(Clip.LOOP_CONTINUOUSLY);
		} catch (UnsupportedAudioFileException | IOException | LineUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Methodes
	public void basculerVueJeu() {

		// Mise en place de la fenetre de jeu
		fenetreAccueil.dispose();
		fenetreJeu = new VueJeu(this);

		fenetreJeu.setNomJoueur(monJeu.getJoueur().getNom(), monJeu.getJoueur().getCamp());
		fenetreJeu.setNomAdversaire(monJeu.getAdversaire().getNom(), monJeu.getAdversaire().getCamp());

		// Traitement du joueur

		switch(monJeu.getJoueur().getCamp()) {
		case ROUGE:
			fenetreJeu.setIconeJoueur("images/rouge/dragon.png");
			break;
		case VERT:
			fenetreJeu.setIconeJoueur("images/vert/dragon.png");
			break;
		case BLEU:
			fenetreJeu.setIconeJoueur("images/bleu/dragon.png");
			break;
		}

		// afficher les monstres du joueur
		dessinerMonstresJoueur();

		// r�f�rencer les coordonn�es des monstres du joueur
		for(int monstre = 0; monstre < monJeu.getJoueur().getCohorte().length; monstre++)  { 
			for(int coordonnee = 0; coordonnee < monJeu.getJoueur().getCohorte()[monstre].getCoordonnees().length; coordonnee++) {
				monJeu.getJoueur().setCaseOccupee(monJeu.getJoueur().getCohorte()[monstre].getCoordonnee(coordonnee), true);
			}
		}

		// Traitement de l'adversaire
		switch(monJeu.getAdversaire().getCamp()) {
		case ROUGE:
			fenetreJeu.setIconeAdversaire("images/rouge/dragon.png");
			break;
		case VERT:
			fenetreJeu.setIconeAdversaire("images/vert/dragon.png");
			break;
		case BLEU:
			fenetreJeu.setIconeAdversaire("images/bleu/dragon.png");
			break;
		}

		// r�f�rencer les coordonn�es des monstres de l'adversaire

		// Boucle pour faire le tour des monstres d'une cohorte
		for(int monstre = 0; monstre < monJeu.getAdversaire().getCohorte().length; monstre++)  {

			// Boucle pour faire le tour des coordonn�es de chaque monstre
			for(int coordonnee = 0; coordonnee < monJeu.getAdversaire().getCohorte()[monstre].getCoordonnees().length; coordonnee++) {

				monJeu.getAdversaire().setCaseOccupee(monJeu.getAdversaire().getCohorte()[monstre].getCoordonnee(coordonnee), true);
			}
		}
		
		afficherTexte("Bienvenue � toi, noble dragon ! Tu as l'initiative, frappe en premier...");
	}

	public void basculerVueAccueil() {
		fenetreJeu.dispose();
		fenetreAccueil = new VueAccueil(this);
		setPartieFinie(false);
		monJeu.setPartieMulti(false);
	}	

	private void dessinerMonstresJoueur() {
		for(int compteur = 0; compteur < monJeu.getJoueur().getCohorte().length; compteur++) {
			switch(monJeu.getJoueur().getMonstre(compteur).getTailleMonstre()) {
			case PETIT:
				fenetreJeu.placerMonstre(fenetreJeu.getGrilleJoueur(),
						monJeu.getJoueur().getMonstre(compteur).getCoordonnee(0),
						monJeu.getJoueur().getMonstre(compteur).getLienImagePresentation());
				break;
			case MOYEN:
				BufferedImage imageMonstreM = new BufferedImage(150, 150, BufferedImage.TYPE_INT_RGB);

				try {
					imageMonstreM = ImageIO.read(new File(monJeu.getJoueur().getMonstre(compteur).getLienImagePresentation()));
				} catch (IOException e) {
					e.printStackTrace();
				}

				int largeurImageM = imageMonstreM.getWidth();
				int hauteurImageM = imageMonstreM.getHeight();

				for(int compteurCases = 0; compteurCases < monJeu.getJoueur().getMonstre(compteur).getCoordonnees().length; compteurCases++) {
					if(compteurCases < 2) 
						fenetreJeu.placerMonstre(fenetreJeu.getGrilleJoueur(),
								monJeu.getJoueur().getMonstre(compteur).getCoordonnee(compteurCases),
								imageMonstreM.getSubimage(compteurCases * (largeurImageM / 2), 0, (largeurImageM / 2), (hauteurImageM / 2)));
					else
						fenetreJeu.placerMonstre(fenetreJeu.getGrilleJoueur(),
								monJeu.getJoueur().getMonstre(compteur).getCoordonnee(compteurCases),
								imageMonstreM.getSubimage((compteurCases - 2) * (largeurImageM / 2), (hauteurImageM / 2), (largeurImageM / 2), (hauteurImageM / 2)));
				}
				break;
			case GRAND:
				BufferedImage imageMonstreG = new BufferedImage(150, 150, BufferedImage.TYPE_INT_RGB);

				try {
					imageMonstreG = ImageIO.read(new File(monJeu.getJoueur().getMonstre(compteur).getLienImagePresentation()));
				} catch (IOException e) {
					e.printStackTrace();
				}

				int largeurImageG = imageMonstreG.getWidth();
				int hauteurImageG = imageMonstreG.getHeight();

				for(int compteurCases = 0; compteurCases < monJeu.getJoueur().getMonstre(compteur).getCoordonnees().length; compteurCases++) {
					if(compteurCases < 3) 
						fenetreJeu.placerMonstre(fenetreJeu.getGrilleJoueur(),
								monJeu.getJoueur().getMonstre(compteur).getCoordonnee(compteurCases),
								imageMonstreG.getSubimage(compteurCases * (largeurImageG / 3), 0, (largeurImageG / 3), (hauteurImageG / 3)));
					else if(compteurCases < 6)
						fenetreJeu.placerMonstre(fenetreJeu.getGrilleJoueur(),
								monJeu.getJoueur().getMonstre(compteur).getCoordonnee(compteurCases),
								imageMonstreG.getSubimage((compteurCases - 3) * (largeurImageG / 3), (hauteurImageG / 3), (largeurImageG / 3), (hauteurImageG / 3)));
					else
						fenetreJeu.placerMonstre(fenetreJeu.getGrilleJoueur(),
								monJeu.getJoueur().getMonstre(compteur).getCoordonnee(compteurCases),
								imageMonstreG.getSubimage((compteurCases - 6) * (largeurImageG / 3), (hauteurImageG / 3)*2, (largeurImageG / 3), (hauteurImageG / 3)));
				}
			}			
		}
	}

	private void dessinerMonstresAdversaire() {
		for(int compteur = 0; compteur < monJeu.getAdversaire().getCohorte().length; compteur++) {
			switch(monJeu.getAdversaire().getMonstre(compteur).getTailleMonstre()) {
			case PETIT:
				fenetreJeu.placerMonstre(fenetreJeu.getGrilleAdversaire(),
						monJeu.getAdversaire().getMonstre(compteur).getCoordonnee(0),
						monJeu.getAdversaire().getMonstre(compteur).getLienImagePresentation());
				break;
			case MOYEN:
				BufferedImage imageMonstreM = new BufferedImage(150, 150, BufferedImage.TYPE_INT_RGB);

				try {
					imageMonstreM = ImageIO.read(new File(monJeu.getAdversaire().getMonstre(compteur).getLienImagePresentation()));
				} catch (IOException e) {
					e.printStackTrace();
				}

				int largeurImageM = imageMonstreM.getWidth();
				int hauteurImageM = imageMonstreM.getHeight();

				for(int compteurCases = 0; compteurCases < monJeu.getAdversaire().getMonstre(compteur).getCoordonnees().length; compteurCases++) {
					if(compteurCases < 2) 
						fenetreJeu.placerMonstre(fenetreJeu.getGrilleAdversaire(),
								monJeu.getAdversaire().getMonstre(compteur).getCoordonnee(compteurCases),
								imageMonstreM.getSubimage(compteurCases * (largeurImageM / 2), 0, (largeurImageM / 2), (hauteurImageM / 2)));
					else
						fenetreJeu.placerMonstre(fenetreJeu.getGrilleAdversaire(),
								monJeu.getAdversaire().getMonstre(compteur).getCoordonnee(compteurCases),
								imageMonstreM.getSubimage((compteurCases - 2) * (largeurImageM / 2), (hauteurImageM / 2), (largeurImageM / 2), (hauteurImageM / 2)));
				}
				break;
			case GRAND:
				BufferedImage imageMonstreG = new BufferedImage(150, 150, BufferedImage.TYPE_INT_RGB);

				try {
					imageMonstreG = ImageIO.read(new File(monJeu.getAdversaire().getMonstre(compteur).getLienImagePresentation()));
				} catch (IOException e) {
					e.printStackTrace();
				}

				int largeurImageG = imageMonstreG.getWidth();
				int hauteurImageG = imageMonstreG.getHeight();

				for(int compteurCases = 0; compteurCases < monJeu.getAdversaire().getMonstre(compteur).getCoordonnees().length; compteurCases++) {
					if(compteurCases < 3) 
						fenetreJeu.placerMonstre(fenetreJeu.getGrilleAdversaire(),
								monJeu.getAdversaire().getMonstre(compteur).getCoordonnee(compteurCases),
								imageMonstreG.getSubimage(compteurCases * (largeurImageG / 3), 0, (largeurImageG / 3), (hauteurImageG / 3)));
					else if(compteurCases < 6)
						fenetreJeu.placerMonstre(fenetreJeu.getGrilleAdversaire(),
								monJeu.getAdversaire().getMonstre(compteur).getCoordonnee(compteurCases),
								imageMonstreG.getSubimage((compteurCases - 3) * (largeurImageG / 3), (hauteurImageG / 3), (largeurImageG / 3), (hauteurImageG / 3)));
					else
						fenetreJeu.placerMonstre(fenetreJeu.getGrilleAdversaire(),
								monJeu.getAdversaire().getMonstre(compteur).getCoordonnee(compteurCases),
								imageMonstreG.getSubimage((compteurCases - 6) * (largeurImageG / 3), (hauteurImageG / 3)*2, (largeurImageG / 3), (hauteurImageG / 3)));
				}
			}			
		}
	}

	public void baisserVieMonstreAdverse(int coordonneeMonstre) {

		int vieMonstre = fenetreJeu.getProgressBarMonstreAdverse(getNumeroMonstreAdverseParCoordonnee(coordonneeMonstre)).getValue();
		if(vieMonstre > 0)
			vieMonstre--;
		fenetreJeu.getProgressBarMonstreAdverse(getNumeroMonstreAdverseParCoordonnee(coordonneeMonstre)).setValue(vieMonstre);
		fenetreJeu.getProgressBarMonstreAdverse(getNumeroMonstreAdverseParCoordonnee(coordonneeMonstre)).setString(
				fenetreJeu.getProgressBarMonstreAdverse(getNumeroMonstreAdverseParCoordonnee(coordonneeMonstre)).getValue()
				+ " / " +
				fenetreJeu.getProgressBarMonstreAdverse(getNumeroMonstreAdverseParCoordonnee(coordonneeMonstre)).getMaximum()
				);
	}

	public void baisserVieMonstreJoueur(int coordonneeMonstre) {
		int vieMonstre = fenetreJeu.getProgressBarMonstreJoueur(getNumeroMonstreJoueurParCoordonnee(coordonneeMonstre)).getValue();
		if(vieMonstre > 0)
			vieMonstre--;
		fenetreJeu.getProgressBarMonstreJoueur(getNumeroMonstreJoueurParCoordonnee(coordonneeMonstre)).setValue(vieMonstre);
		fenetreJeu.getProgressBarMonstreJoueur(getNumeroMonstreJoueurParCoordonnee(coordonneeMonstre)).setString(
				fenetreJeu.getProgressBarMonstreJoueur(getNumeroMonstreJoueurParCoordonnee(coordonneeMonstre)).getValue()
				+ " / " +
				fenetreJeu.getProgressBarMonstreJoueur(getNumeroMonstreJoueurParCoordonnee(coordonneeMonstre)).getMaximum()
				);
	}

	public void verifierVictoire() {
		BufferedImage imageBase;
		ImageIcon icone = new ImageIcon();
		try {
			imageBase = ImageIO.read(new File("images/Illustrations/04_touche.png"));

			icone = new ImageIcon(imageBase.getScaledInstance(100, 100, java.awt.Image.SCALE_SMOOTH));

		} catch (IOException e) {
			e.printStackTrace();
		}

		if(getViesMonstresAdverses() == 0) {
			setPartieFinie(true);
			lecteurSon.jouer("sons/Victoire.wav", false);
			JOptionPane.showMessageDialog(null,
					"F�licitations !\n"
					+ "Vous avez battu votre adversaire.\n"
					+ "(cliquer sur le bouton vous renvoie � l'accueil)",
					"Victoire", 
					JOptionPane.INFORMATION_MESSAGE,
					icone);
			basculerVueAccueil();
		} else if(getViesMonstresJoueur() == 0) {
			dessinerMonstresAdversaire();
			setPartieFinie(true);
			lecteurSon.jouer("sons/Rire.wav", false);
			JOptionPane.showMessageDialog(null,
					"D�sol� !\n Vous avez �t� vaincu.\n"
					+ "Vous pouvez d�placer cette fen�tre pour voir o� se trouvaient les cr�atures adverses\n"
					+ "(cliquer sur le bouton vous renvoie � l'accueil)",
					"D�faite", 
					JOptionPane.INFORMATION_MESSAGE,
					icone);
			basculerVueAccueil();
		}
	}

	public int choixCaseAleatoire() {
		int tirage = 0; 
		int x = (int) (Math.random() * (14));
		int y = (int) (Math.random() * (14));
		tirage = x + 14 * y;
		return tirage;
	}

	public void tirAdverseAleatoire() {

		boolean attaqueValide = false;

		while(!attaqueValide) {
			int tentativeTir = choixCaseAleatoire();

			if(fenetreJeu.isBoutonActifJoueur(tentativeTir)) {
				ImageIcon icone = new ImageIcon();

				if(isCaseJoueurOccupee(tentativeTir)) {
					icone = new ImageIcon(
							((new ImageIcon("images/Illustrations/04_touche.png")).getImage()).getScaledInstance(fenetreJeu.getBoutonJoueur(tentativeTir).getWidth(),
									fenetreJeu.getBoutonJoueur(tentativeTir).getHeight(), java.awt.Image.SCALE_SMOOTH));
					baisserVieMonstreJoueur(tentativeTir);
					lecteurSon.jouer("sons/Souffrance.wav", false);
					afficherTexte("Votre adversaire a touch� l'une de vos cr�atures");
				} else {
					switch(getCampAdversaire()) {
					case ROUGE:
						icone = new ImageIcon(
								((new ImageIcon("images/rouge/effect.png")).getImage()).getScaledInstance(fenetreJeu.getBoutonJoueur(tentativeTir).getWidth(),
										fenetreJeu.getBoutonJoueur(tentativeTir).getHeight(), java.awt.Image.SCALE_SMOOTH));
						break;
					case VERT:
						icone = new ImageIcon(((new ImageIcon("images/vert/effect.png")).getImage()).getScaledInstance(fenetreJeu.getBoutonJoueur(tentativeTir).getWidth(),
								fenetreJeu.getBoutonJoueur(tentativeTir).getHeight(), java.awt.Image.SCALE_SMOOTH));
						break;
					case BLEU:
						icone = new ImageIcon(((new ImageIcon("images/bleu/effect.png")).getImage()).getScaledInstance(fenetreJeu.getBoutonJoueur(tentativeTir).getWidth(),
								fenetreJeu.getBoutonJoueur(tentativeTir).getHeight(), java.awt.Image.SCALE_SMOOTH));
					}
					afficherTexte("Votre adversaire n'a touch� aucune de vos cr�atures");
				}
				fenetreJeu.getBoutonJoueur(tentativeTir).setIcon(icone);
				fenetreJeu.getBoutonJoueur(tentativeTir).setDisabledIcon(icone);
				fenetreJeu.getBoutonJoueur(tentativeTir).setEnabled(false);
				attaqueValide = true;
			}
		}
		monJeu.avancerTour();
	}

	public void reinitialiserPartie(String nouveauNom, Camp campJoueur) {
		monJeu.nouveauJeu(nouveauNom, campJoueur);
	}

	public void changerNomJoueur(String nouveauNom) {
		monJeu.getJoueur().setNom(nouveauNom);
	}

	// Accesseur et Mutateurs

	public VueAccueil getFenetreAccueil() {
		return fenetreAccueil;
	}

	public VueJeu getFenetreJeu() {
		return fenetreJeu;
	}

	public Jeu getMonJeu() {
		return monJeu;
	}

	public String getNomJoueur() {
		return nomJoueur;
	}

	public void setNomJoueur(String nomJoueur) {
		this.nomJoueur = nomJoueur;
	}

	public boolean isPanneauJoueur(PanneauAdaptatif panneauATester) {
		boolean resultat = false;

		if(panneauATester == fenetreJeu.getGrilleJoueur())
			resultat = true;

		return resultat;
	}	

	public boolean isTourJoueur() {
		return tourJoueur;
	}

	public Camp getCampJoueur() {
		return monJeu.getJoueur().getCamp();		
	}

	public Camp getCampAdversaire() {
		return monJeu.getAdversaire().getCamp();		
	}

	public int getNumeroMonstreAdverseParCoordonnee(int coordonneeMonstre) {
		int numeroMonstreTrouve = -1;
		// Boucle pour faire le tour des monstres d'une cohorte
		for(int monstre = 0; monstre < monJeu.getAdversaire().getCohorte().length; monstre++)  {

			// Boucle pour faire le tour des coordonn�es de chaque monstre
			for(int coordonnee = 0; coordonnee < monJeu.getAdversaire().getCohorte()[monstre].getCoordonnees().length; coordonnee++) {
				if(monJeu.getAdversaire().getCohorte()[monstre].getCoordonnees()[coordonnee] == coordonneeMonstre)
					numeroMonstreTrouve = monstre;
			}
		}
		return numeroMonstreTrouve;
	}

	public int getNumeroMonstreJoueurParCoordonnee(int coordonneeMonstre) {
		int numeroMonstreTrouve = -1;
		// Boucle pour faire le tour des monstres d'une cohorte
		for(int monstre = 0; monstre < monJeu.getJoueur().getCohorte().length; monstre++)  {

			// Boucle pour faire le tour des coordonn�es de chaque monstre
			for(int coordonnee = 0; coordonnee < monJeu.getJoueur().getCohorte()[monstre].getCoordonnees().length; coordonnee++) {
				if(monJeu.getJoueur().getCohorte()[monstre].getCoordonnees()[coordonnee] == coordonneeMonstre)
					numeroMonstreTrouve = monstre;
			}
		}
		return numeroMonstreTrouve;
	}

	public boolean isPartieFinie() {
		return partieFinie;
	}

	public void setPartieFinie(boolean partieFinie) {
		this.partieFinie = partieFinie;
	}

	public void setTourJoueur(boolean tourJoueur) {
		this.tourJoueur = tourJoueur;
	}

	public boolean isCaseAdversaireOccupee(int indexCase) {
		return monJeu.getAdversaire().isCaseOccupee(indexCase);
	}


	public boolean isCaseJoueurOccupee(int indexCase) {
		return monJeu.getJoueur().isCaseOccupee(indexCase);
	}

	public int getViesMonstresAdverses() {
		int total = 0;
		for(int compteur = 0; compteur < 4; compteur++) {
			total += fenetreJeu.getProgressBarMonstreAdverse(compteur).getValue();
		}
		return total;
	}

	public int getViesMonstresJoueur() {
		int total = 0;
		for(int compteur = 0; compteur < 4; compteur++) {
			total += fenetreJeu.getProgressBarMonstreJoueur(compteur).getValue();
		}
		return total;
	}
	
	public LecteurDeSon getLecteurSon() {
		return lecteurSon;
	}

	public void afficherTexte(String texte) {
		fenetreJeu.ajouterTexteLog("Tour " + monJeu.getTour() + " - Ma�tre du jeu : " + texte);
	}
}