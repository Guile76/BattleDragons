package modeles;

import javax.swing.ImageIcon;

import enumerations.TailleMonstre;

public class Monstre {
	// Variables
	private TailleMonstre tailleMonstre;
	private int[] coordonnees;
	private boolean isMort;
	private String lienImagePresentation;
	private ImageIcon imagePresentation;
	
	// Constructeurs
	
	public Monstre(TailleMonstre tailleMonstre, String dossier, String lienImagePresentation, int coordonneeBase) {
		this.tailleMonstre = tailleMonstre;
		this.lienImagePresentation = (dossier + lienImagePresentation);
		
		switch(tailleMonstre) {
		case PETIT :
			coordonnees = new int[1];
			coordonnees[0] = coordonneeBase;
			break;
		case MOYEN :
			coordonnees = new int[4];
			coordonnees[0] = coordonneeBase;
			for(int compteur = 1; compteur < coordonnees.length; compteur++) {
				if(compteur < 2)
					coordonnees[compteur] = coordonneeBase + compteur;
				else
					coordonnees[compteur] = coordonneeBase + 12 + compteur;
			}
			break;
		case GRAND :
			coordonnees = new int[9];
			coordonnees[0] = coordonneeBase;
			for(int compteur = 1; compteur < coordonnees.length; compteur++) {
				if(compteur < 3)
					coordonnees[compteur] = coordonneeBase + compteur;
				else if (compteur < 6)
					coordonnees[compteur] = coordonneeBase + 11 + compteur;
				else 
					coordonnees[compteur] = coordonneeBase + 22 + compteur;
			}			
		}
		
		imagePresentation = new ImageIcon(lienImagePresentation);
	}
	
	// Accesseurs et Mutateurs
	public TailleMonstre getTailleMonstre() {
		return tailleMonstre;
	}

	public int[] getCoordonnees() {
		return coordonnees;
	}
	
	public int getCoordonnee(int indexCoordonnee) {
		return coordonnees[indexCoordonnee];
	}
	
	public void setCoordonnee(int indexCoordonnee, int valeurCoordonnee) {
		coordonnees[indexCoordonnee] = valeurCoordonnee;
	}

	public boolean isMort() {
		return isMort;
	}

	public void setMort(boolean isMort) {
		this.isMort = isMort;
	}

	public String getLienImagePresentation() {
		return lienImagePresentation;
	}

	public ImageIcon getImagePresentation() {
		return imagePresentation;
	}
}
