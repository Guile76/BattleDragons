package modeles;

import enumerations.Camp;

public class Jeu {
	
	// Variables
	private boolean partieMulti = false;
	private Joueur joueur;
	private Joueur adversaire;
	private int tour = 1;
	
	// Constructeur
	public Jeu() {
		
	}
	
	// Accesseurs et Mutateurs
	public boolean isPartieMulti() {
		return partieMulti;
	}

	public Joueur getJoueur() {
		return joueur;
	}

	public Joueur getAdversaire() {
		return adversaire;
	}

	public void setPartieMulti(boolean partieMulti) {
		this.partieMulti = partieMulti;
	}
	
	public int getTour() {
		return tour;
	}
	
	// Méthodes
	
	public void avancerTour() {
		tour++;
	}
	

	public void nouveauJeu(String nomJoueur, Camp campJoueur) {
if(!partieMulti) {
			
			// Joueur1			
			joueur = new Joueur(nomJoueur, false, campJoueur);
			
			// Joueur 2 = ordinateur
			long tirageCampOrdi = Math.round((2 * Math.random()) + 1);
			String nomOrdi = "";
			Camp campOrdi = Camp.ROUGE;
			switch((int)tirageCampOrdi) {
			case 1:
				campOrdi = Camp.ROUGE;
				nomOrdi = "Ashardalon";
				break;
			case 2:
				campOrdi = Camp.VERT;
				nomOrdi = "Venomfang";
				break;
			case 3:
				campOrdi = Camp.BLEU;
				nomOrdi = "Iymrith";
				break;
			}
			adversaire = new Joueur(nomOrdi, true, campOrdi);
		}
	}

}
