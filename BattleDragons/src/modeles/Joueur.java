package modeles;

import enumerations.Camp;
import enumerations.TailleMonstre;

public class Joueur {
	// Variables
	private String nom;
	private boolean ordinateur;
	private Monstre[] cohorte;
	private boolean[] casesOccupees;
	private Camp camp;
	private String dossier;

	// Constructeur
	public Joueur(String nom, boolean ordinateur, Camp camp) {
		this.nom = nom;
		this.ordinateur = ordinateur;
		this.camp = camp;
		switch(camp) {
		case ROUGE:
			dossier = "images/rouge/";
			break;
		case VERT:
			dossier = "images/vert/";
			break;
		case BLEU:
			dossier = "images/bleu/";
			break;
		}

		casesOccupees = new boolean[14 * 14];
		for(int compteur = 0; compteur < (14 * 14); compteur++) {
			casesOccupees[compteur] = false;
		}

		creerCohorte();
	}

	// Méthodes
	private void creerCohorte() {
		cohorte = new Monstre[4];

		cohorte[3] = new Monstre(TailleMonstre.GRAND, dossier, "3X3.png", nombreAleatoire(TailleMonstre.GRAND));
		integrerMonstre(cohorte[3]);
		cohorte[2] = new Monstre(TailleMonstre.MOYEN, dossier, "2X2B.png", nombreAleatoire(TailleMonstre.MOYEN));
		integrerMonstre(cohorte[2]);
		cohorte[1] = new Monstre(TailleMonstre.MOYEN, dossier, "2X2A.png", nombreAleatoire(TailleMonstre.MOYEN));
		integrerMonstre(cohorte[1]);
		cohorte[0] = new Monstre(TailleMonstre.PETIT, dossier, "1X1.png", nombreAleatoire(TailleMonstre.PETIT));
		integrerMonstre(cohorte[0]);

	}

	private void integrerMonstre(Monstre monstre) {

		for(int compteur = 0; compteur < monstre.getCoordonnees().length; compteur++) {
			setCaseOccupee(monstre.getCoordonnees()[compteur], true);
		}
	}

	// Accesseurs
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public boolean isOrdinateur() {
		return ordinateur;
	}

	public Monstre[] getCohorte() {
		return cohorte;
	}

	public Monstre getMonstre(int index) {
		return cohorte[index];
	}

	public Camp getCamp() {
		return camp;
	}

	public boolean[] getCasesOccupees() {
		return casesOccupees;
	}

	public void setCaseOccupee(int indexCase, boolean valeurOccupee) {
		casesOccupees[indexCase] = valeurOccupee;
	}

	public boolean isCaseOccupee(int indexCase) {
		return casesOccupees[indexCase];
	}

	public int nombreAleatoire(TailleMonstre tailleMonstre) {
		int tirage = 0;
		int compteur;
		boolean valide = true;
		boolean fini = false;
		int taille=0;
		int ligne = -1;

		switch(tailleMonstre){
		case PETIT:
			taille = 1;
			break;
		case MOYEN:
			taille = 2;
			break;
		case GRAND:
			taille = 3;
		}

		while(!fini) {

			int x = (int) (Math.random() * (14 - taille));
			int y = (int) (Math.random() * (14 - taille));
			tirage = x + 14 * y;
			valide = true;
			
			for(compteur = 0; compteur < taille*taille && valide; compteur++) {
				if(compteur % taille == 0) {
					ligne++;
				}
				if(isCaseOccupee(tirage + compteur + (14 * ligne- ligne*taille )))
					valide = false;
			}

			if(valide)
				fini = true;
		}
		return tirage;
	}
}
